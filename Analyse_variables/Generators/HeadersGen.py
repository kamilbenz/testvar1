import json
import requests

class HeadersGen():
    """
    This class is used to generate a header to the request
    """
    def __init__(self, lenght):
        """
        Initialization method.
        """
        self.headers = {
            'Content-Type': "application/json",
            'User-Agent': "PostmanRuntime/7.20.1",
            'Accept': "*/*",
            'Cache-Control': "no-cache",
            'Postman-Token': "0b182d5d-c8f1-42eb-9ee7-b306432389fd,70f8d718-9106-4da0-8516-8ee9cfa2081a",
            'Host': "sport2000-stage.lineup7-platform.fr",
            'Accept-Encoding': "gzip, deflate",
            'Connection': "keep-alive",
            'cache-control': "no-cache"
            }

        from Generators.TokenGen import TokenGen
        token_import = TokenGen()
        self.headers['Authorization'] = token_import.token
        self.headers['Content-Length'] = str(lenght)