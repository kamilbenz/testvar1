import json
import requests

class TokenGen():
    """
    This class is used to generate a token for the request.
    """
    def __init__(self):
        """
        Initialization method.
            """
        self.url = "https://auth-sport2000.lineup7-platform.fr/oauth2/token"

        self.querystring = {"grant_type":"client_credentials"}

        self.payload = "client_id=4ro1vdlm1afso2f277emgr1c3h&client_secret=k0puoujhcpog8675ujoemctojlllkh1cqqo7cfpbqnsdgjpijll"
        self.headers = {
            'Content-Type': "application/x-www-form-urlencoded",
            'User-Agent': "PostmanRuntime/7.20.1",
            'Accept': "*/*",
            'Cache-Control': "no-cache",
            'Postman-Token': "de3a7980-46bd-47db-8b5d-e5be397f67f6,dce05c51-49d0-4777-8f6d-b8d0057e8c70",
            'Host': "auth-sport2000.lineup7-platform.fr",
            'Accept-Encoding': "gzip, deflate",
            'Content-Length': "102",
            'Cookie': "XSRF-TOKEN=19c0baa8-1d24-482b-9ad8-6c4fcf6935c9",
            'Connection': "keep-alive",
            'cache-control': "no-cache"
            }

        self.token = json.loads(requests.request("POST", url = self.url, data = self.payload, headers = self.headers, params = self.querystring).text)['access_token']