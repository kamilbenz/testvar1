import json
import requests
import numpy as np

class TestLoop:
    """
    This class concerns only POST requests
    """
    global tab_var_def
    tab_var_def = ['a','1997-12-22','  e s   sa y o ns d es  esp  a  ces ','2019-11-03T15:36','kamil.benzakri@lineup7.fr',32.42938492384729849283652936523,1234,9223372036854775808,'hello','','azaRER1231',None,'\'',True,"*",'34,45',34.45,-55.32,12345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890]


    def __init__(self, payload, endpoint_name, url):
        """
        Initialization method. All useful data for the request is assigned in this method (calling other classes)
        """
        self.payload = payload
        self.endpoint_name = endpoint_name
        self.url = url


    def flatten_dico(self, dico):
        """
        This function flattens any dictionary (even if containing dictionaries itself) into a list. When flattened, all keys are lost but that doesn't disturb the operation
        """
        i = 0
        L = []
        for elt in dico:
            if type(dico[elt]) == list:
                a = self.flatten_dico(dico[elt][0])
                L += a
            else:
                L.append(elt)
        return L


    def affect_next_var(self, dico, memory, new_val):
        """
        Function that takes 3 parameters : dico contains the payload, memory is a list containing the path to the last changed variable, as dico, it's a dictionary which also undergoes modifications in this function, so be careful with the memory list manipulation. The function is recursive so as it's suitable for dictionaries inside dictionaries (and so on) cases too.
        """
        var = iter(dico)
        boolean, bool_1 = False, False
        active_var = next(var)
        cache = {}
        while cache != memory:
            cache[active_var] = dico[active_var]
            try:
                active_var = next(var)
                boolean = False
            except StopIteration:
                return (True, memory, True)
            if type(dico[active_var]) == list:
                if active_var in memory:
                    (boolean, mem, bool_1) = self.affect_next_var(dico[active_var][0], memory[active_var][0], new_val)
                    if bool_1 == False:
                        memory.pop(active_var)
                else:
                    (boolean, mem, bool_1) = self.affect_next_var(dico[active_var][0], {}, new_val)
        if boolean == True:
            try:
                active_var = next(var)
            except:
                return
        if type(dico[active_var]) == list:
            memory[active_var] = [mem]
        else:
            memory[active_var] = dico[active_var]
            dico[active_var] = new_val.pop()
        return (boolean, memory, bool_1)


    def generate_headers(self, lenght):
        """
        This method is used to generate a token through TokenGen class.
        """
        from Generators.HeadersGen import HeadersGen
        header_import = HeadersGen(lenght)
        self.headers = header_import.headers


    def run_analysis(self, var_tab = tab_var_def, Error_Code_Only = False):
        """
        This function runs the test cycle
        """
        rapport = open('Rapports/rapport_testvar_' + self.endpoint_name + '.txt','w')
        rapport.write('variable remplacée\tvariable testée\tmessage d\'erreur\tcode erreur\n')
        dico = json.loads(self.payload)
        flattened_dico = self.flatten_dico(dico)
        print('beginning')
        for var in var_tab:
            memory = {}
            for i in range(len(flattened_dico)):
                dico = json.loads(self.payload)
                self.affect_next_var(dico,memory,[var])
                print(flattened_dico[i])
                print(var)
                if Error_Code_Only == False:
                    self.generate_headers(len(str(self.payload)))
                    response = requests.request("POST", self.url, data = json.dumps(dico), headers = self.headers)
                    result = response.text
                #     print(result,'\n')
                # else:
                #     print(occured_error(dico),'\n')
                if var == '':
                    rapport.write(str(flattened_dico[i]) + '\t' + '""' + '\t' + result + '\t' + str(response.status_code) + '\n')
                else:
                    rapport.write(str(flattened_dico[i]) + '\t' + str(var) + '\t' + result + '\t' + str(response.status_code) + '\n')
        rapport.close()