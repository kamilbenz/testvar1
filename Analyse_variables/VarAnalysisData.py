class VarAnalysisData:
    """
    This class creates the analysis object
    """
    def __init__(self, source, request_id, request_type):
        """
        Initialization method. Assignes all useful data to the object. only request_data is useful, but the rest could be later.
        """
        self.source = source
        self.request_id = request_id
        self.request_type = request_type

        from Bodies.BodyImport import BodyImport
        payload_import = BodyImport(request_id)
        self.request_data = payload_import.payload

        from Urls.UrlImport import UrlImport
        url_import = UrlImport(request_id)
        self.url = url_import.url

    def launch_test(self):
        """
        This method launches the test cycle
        """
        print(self.request_data)
        print(self.request_id)
        print(self.url)

        from TestLoop import TestLoop
        TestLoop_import = TestLoop(self.request_data, self.request_id, self.url)
        TestLoop_import.run_analysis()