import json

class BodyImport():
    """
    This class imports the json payload file requested from the /Bodies folder
    """
    def __init__(self, request_id):
        body_file = open('Bodies/' + str(request_id) + '.txt','r')
        try:
            self.payload = json.dumps(json.loads(''.join(body_file.readlines())))
        except:
            print('Error : Not Found ?')