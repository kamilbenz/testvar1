class UrlImport:
    """
    This class can be used to import the url to use for the request
    """
    def __init__(self, request_id):
        url_file = open('Urls/' + str(request_id) + '.txt','r')
        try:
            self.url = url_file.readline()
        except:
            print('Error : Not Found ?')